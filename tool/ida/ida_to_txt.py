# IDA to txt script
# @author ShadowHunterRus
# @author Mikhail Paulyshka
# @license GPLv3

from idaapi   import *
from idautils import *
from idc      import *

import sigutils
reload(sigutils)

import simplejson
import os

jsonfolder = os.path.dirname(os.path.dirname(__file__))+"../../data/lib_python/json/"
wotversion = sigutils.getFileVersion()

known_functions = list()
known_structures =list()

for filename in os.listdir(jsonfolder):
    if filename.endswith(".json"):
        with open(jsonfolder+filename, 'r') as f:
            json_file = simplejson.load(f)

        for wot_function in json_file['functions']:
            offset = wot_function['offsets'].get(wotversion)
            if offset is not None:
                known_functions.append(wot_function['name'])

        for wot_structure in json_file['structures']:
            offset = wot_structure['offsets'].get(wotversion)
            if offset is not None:
                known_structures.append(wot_structure['name'])
