﻿using System.IO;
using System.Reflection;

namespace XFWNative.Libpython.Update
{
    class Program
    {
        static void Main(string[] args)
        {
            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));

            //new Updater("../../../data/lib_python/", 64).Process();
            new Updater("../../../data/lib_python/", 32).Process();

        }
    }
}
