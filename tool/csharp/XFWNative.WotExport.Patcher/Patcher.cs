﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using XFWNative.Common.Extensions;
using XFWNative.WotExport.Common;

namespace XFWNative.WotExport.Patcher
{
    class Patcher
    {
        private string symbolFile;
        private string binaryFile;

        public List<SymbolRecord> Symbols = new List<SymbolRecord>();


        public Patcher(string symbolFile, string binaryFile)
        {
            this.symbolFile = symbolFile;
            this.binaryFile = binaryFile;

            loadSymbols();
        }

        private void loadSymbols()
        {
            var filelines = File.ReadAllLines(symbolFile);
            foreach (var line in filelines)
            {
                Symbols.Add(new SymbolRecord(line));
            }
        }


        internal void Patch()
        {
            var bytes = File.ReadAllBytes(binaryFile);
            foreach (var symbol in Symbols)
            {
                if (symbol.Function.Replace('@', '^') != symbol.Function)
                {
                    bytes = bytes.ReplaceBytes(Encoding.ASCII.GetBytes(symbol.Function.Replace('@','^')), Encoding.ASCII.GetBytes(symbol.Function));
                }
            }

            File.WriteAllBytes(binaryFile, bytes);
        }
    }
}
