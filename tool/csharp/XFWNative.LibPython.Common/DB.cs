﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Newtonsoft.Json;

namespace XFWNative.Libpython.Common
{
    public class DB
    {
        #region Properties

        private Dictionary<string, Data.Signatures.Root> DataSignatures { get; } = new Dictionary<string, Data.Signatures.Root>();
        public Dictionary<string, ConcurrentDictionary<string, string>> DataOffsets32 { get; private set; } = new Dictionary<string, ConcurrentDictionary<string, string>>();
        public Dictionary<string, ConcurrentDictionary<string, string>> DataOffsets64 { get; private set; } = new Dictionary<string, ConcurrentDictionary<string, string>>();

        public List<string> InlinedFunctions { get;  } = new List<string>();

        private string DataFolder { get; }
        private string JsonFolder { get; }
        private string FunctionsFolder { get; }

        #endregion

        #region Initialization

        public DB(string folder)
        {
            DataFolder = folder;
            JsonFolder = Path.Combine(DataFolder, "json\\");
            FunctionsFolder = Path.Combine(DataFolder, "c\\");

            LoadInlineNames();
            LoadJsonData();
        }

        private void LoadInlineNames()
        {
            foreach (var file in Directory.GetFiles(FunctionsFolder, "*.*"))
            {
                InlinedFunctions.Add(Path.GetFileNameWithoutExtension(file));
            }
        }

        #endregion

        #region Load/Save

        private void LoadJsonData()
        {
            if (!Directory.Exists(JsonFolder))
            {
                Console.WriteLine($"ERROR: {JsonFolder} does not exists");
                return;
            }

            foreach (string file in Directory.GetFiles(JsonFolder))
            {
                if (Path.GetExtension(file).ToLower() != ".json")
                    continue;

                var json = JsonConvert.DeserializeObject<Common.Data.Signatures.Root>(File.ReadAllText(file));

                foreach (var function in json.Functions)
                {
                    if (file.ToLower().Contains("internal"))
                        function.IsInternal = true;

                    if (InlinedFunctions.Contains(function.Name))
                    {
                        function.SetInlined(32, true);
                        function.SetInlined(64, true);
                    }
                }

                DataSignatures[file] = json;

            }
            LoadOffsets();
        }

        private void LoadOffsets()
        {
            DataOffsets32.Clear();
            DataOffsets64.Clear();

            var offsets32_dir = Path.Combine(JsonFolder, "offsets_32bit//");
            if (Directory.Exists(offsets32_dir))
            {
                foreach (string file in Directory.GetFiles(offsets32_dir))
                {
                    if (Path.GetExtension(file).ToLower() != ".json")
                    {
                        continue;
                    }

                    DataOffsets32[Path.GetFileNameWithoutExtension(file)] = JsonConvert.DeserializeObject<ConcurrentDictionary<string, string>>(File.ReadAllText(file));
                }
            }


            var offsets64_dir = Path.Combine(JsonFolder, "offsets_64bit//");
            if (Directory.Exists(offsets64_dir))
            {
                foreach (string file in Directory.GetFiles(offsets64_dir))
                {
                    if (Path.GetExtension(file).ToLower() != ".json")
                    {
                        continue;
                    }

                    DataOffsets64[Path.GetFileNameWithoutExtension(file)] = JsonConvert.DeserializeObject<ConcurrentDictionary<string, string>>(File.ReadAllText(file));
                }
            }
        }

        public void SaveJsonData()
        {
            foreach (var kvp in DataSignatures)
            {
                File.WriteAllText(kvp.Key,JsonConvert.SerializeObject(kvp.Value, Formatting.Indented));
            }

            SaveOffsets();
        }

        private void SaveOffsets()
        {
            foreach(var kvp in DataOffsets32)
            {
                File.WriteAllText(Path.Combine(JsonFolder, $"offsets_32bit//{kvp.Key}.json"), JsonConvert.SerializeObject(kvp.Value, Formatting.Indented));
            }

            foreach (var kvp in DataOffsets64)
            {
                File.WriteAllText(Path.Combine(JsonFolder, $"offsets_64bit//{kvp.Key}.json"), JsonConvert.SerializeObject(kvp.Value, Formatting.Indented));
            }

        }

        #endregion

        #region Data

        public IEnumerable<Data.Signatures.Function> GetFunctions()
        {
            return DataSignatures.SelectMany(x => x.Value.Functions);
        }

        public IEnumerable<Data.Signatures.Structure> GetStructures()
        {
            return DataSignatures.SelectMany(x => x.Value.Structures);
        }

        public ConcurrentDictionary<string,string> GetOffsetsForVersion(string version, int bitness)
        {
            var obj = bitness == 64 ? DataOffsets64 : DataOffsets32;

            if (!obj.ContainsKey(version))
            {
                obj[version] = new ConcurrentDictionary<string, string>();
            }

            return obj[version];
        }

        #endregion
    }
}
