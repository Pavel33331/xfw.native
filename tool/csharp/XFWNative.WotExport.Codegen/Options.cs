﻿using CommandLine;

namespace XFWNative.WotExport.Codegen
{
    class Options
    {
        [Option('d', "data_dir", Required = true, HelpText = "Data Directory.")]
        public string DataDirectory { get; set; }

        [Option('o', "output_dir", Required = true, HelpText = "Output Directory.")]
        public string OutputDirectory { get; set; }

    }
}
