#define Py_BUILD_CORE 1
#include "Python.h"
#include "windows.h"

PyAPI_FUNC(int) PyThread_create_key(void);

int
PyThread_create_key(void)
{
    return (int) TlsAlloc();
}