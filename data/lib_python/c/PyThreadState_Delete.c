#define Py_BUILD_CORE 1
#include "Python.h"
#include "pythread.h"

//CPython 2.7.18, pystate.c, L#31
extern PyThread_type_lock* head_mutex;
#define head_mutex (*head_mutex)

//CPython 2.7.18, pystate.c, L#33
#define HEAD_LOCK() PyThread_acquire_lock(head_mutex, WAIT_LOCK)
#define HEAD_UNLOCK() PyThread_release_lock(head_mutex)


//CPython 2.7.18, pystate.c, L#39
extern PyInterpreterState** autoInterpreterState;
#define autoInterpreterState (*autoInterpreterState)


//CPython 2.7.18, pystate.c, L#40
extern int* autoTLSkey;
#define autoTLSkey (*autoTLSkey)


//CPython 2.7.18, pystate.c, L#258
static void
tstate_delete_common(PyThreadState *tstate)
{
    PyInterpreterState *interp;
    PyThreadState **p;
    PyThreadState *prev_p = NULL;
    if (tstate == NULL)
        Py_FatalError("PyThreadState_Delete: NULL tstate");
    interp = tstate->interp;
    if (interp == NULL)
        Py_FatalError("PyThreadState_Delete: NULL interp");
    HEAD_LOCK();
    for (p = &interp->tstate_head; ; p = &(*p)->next) {
        if (*p == NULL)
            Py_FatalError(
                "PyThreadState_Delete: invalid tstate");
        if (*p == tstate)
            break;
        /* Sanity check.  These states should never happen but if
         * they do we must abort.  Otherwise we'll end up spinning in
         * in a tight loop with the lock held.  A similar check is done
         * in thread.c find_key().  */
        if (*p == prev_p)
            Py_FatalError(
                "PyThreadState_Delete: small circular list(!)"
                " and tstate not found.");
        prev_p = *p;
        if ((*p)->next == interp->tstate_head)
            Py_FatalError(
                "PyThreadState_Delete: circular list(!) and"
                " tstate not found.");
    }
    *p = tstate->next;
    HEAD_UNLOCK();
    free(tstate);
}


//CPython 2.7.18, pystate.c, L#296
void
PyThreadState_Delete(PyThreadState *tstate)
{
    if (tstate == _PyThreadState_Current)
        Py_FatalError("PyThreadState_Delete: tstate is still current");
    tstate_delete_common(tstate);
#ifdef WITH_THREAD
    if (autoInterpreterState && PyThread_get_key_value(autoTLSkey) == tstate)
        PyThread_delete_key_value(autoTLSkey);
#endif /* WITH_THREAD */
}
