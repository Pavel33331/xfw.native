#define Py_BUILD_CORE 1
#include "Python.h"

PyObject *
PyMethod_Class(PyObject *im)
{
    if (!PyMethod_Check(im)) {
        PyErr_BadInternalCall();
        return NULL;
    }
    return ((PyMethodObject *)im)->im_class;
}