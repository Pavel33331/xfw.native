#define Py_BUILD_CORE 1
#include "Python.h"
#include "pymem.h"

PyAPI_FUNC(void *) PyMem_Malloc(size_t);

/* Python's malloc wrappers (see pymem.h) */

void *
PyMem_Malloc(size_t nbytes)
{
    return PyMem_MALLOC(nbytes);
}