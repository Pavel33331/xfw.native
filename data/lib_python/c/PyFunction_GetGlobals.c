#define Py_BUILD_CORE 1
#include "Python.h"

#undef PyFunction_GetGlobals
PyAPI_FUNC(PyObject *) PyFunction_GetGlobals(PyObject *);

PyObject *
PyFunction_GetGlobals(PyObject *op)
{
    if (!PyFunction_Check(op)) {
        PyErr_BadInternalCall();
        return NULL;
    }
    return ((PyFunctionObject *) op) -> func_globals;
}
