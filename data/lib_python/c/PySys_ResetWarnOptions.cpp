#define Py_BUILD_CORE 1

#include "Python.h"

PyAPI_FUNC(void) PySys_ResetWarnOptions(void);

extern "C" size_t warnoptions;

void
PySys_ResetWarnOptions(void)
{
    if (*reinterpret_cast<PyObject**>(warnoptions) == NULL || !PyList_Check(*reinterpret_cast<PyObject**>(warnoptions)))
        return;

    PyList_SetSlice(*reinterpret_cast<PyObject**>(warnoptions), 0, PyList_GET_SIZE(*reinterpret_cast<PyObject**>(warnoptions)), NULL);
}
