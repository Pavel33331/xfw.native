#define Py_BUILD_CORE 1
#include "Python.h"

PyAPI_FUNC(void) PyFloat_AsString(char*, PyFloatObject *v);

void
PyFloat_AsString(char *buf, PyFloatObject *v)
{
    char *tmp = PyOS_double_to_string(v->ob_fval, 'g',
                    PyFloat_STR_PRECISION,
                    Py_DTSF_ADD_DOT_0, NULL);
    strcpy(buf, tmp);
    PyMem_Free(tmp);
}