#define Py_BUILD_CORE 1
#include "Python.h"
#include "pymem.h"

PyAPI_FUNC(void *) PyMem_Realloc(void *, size_t);

/* Python's malloc wrappers (see pymem.h) */

void *
PyMem_Realloc(void *p, size_t nbytes)
{
    return PyMem_REALLOC(p, nbytes);
}