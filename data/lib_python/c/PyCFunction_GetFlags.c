#define Py_BUILD_CORE 1
#include "Python.h"

PyAPI_FUNC(int) PyCFunction_GetFlags(PyObject *);

int
PyCFunction_GetFlags(PyObject *op)
{
    if (!PyCFunction_Check(op)) {
        PyErr_BadInternalCall();
        return -1;
    }
    return ((PyCFunctionObject *)op) -> m_ml -> ml_flags;
}