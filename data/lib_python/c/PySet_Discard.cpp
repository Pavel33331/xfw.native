#define Py_BUILD_CORE 1
#define DISCARD_NOTFOUND 0
#define DISCARD_FOUND 1

#include "Python.h"

#undef PySet_Discard
PyAPI_FUNC(int) PySet_Discard(PyObject *set, PyObject *key);

extern "C" size_t dummy;

static int
set_discard_key(PySetObject *so, PyObject *key)
{
    register long hash;
    register setentry *entry;
    PyObject *old_key;

    assert (PyAnySet_Check(so));
    if (!PyString_CheckExact(key) ||
        (hash = ((PyStringObject *) key)->ob_shash) == -1) {
        hash = PyObject_Hash(key);
        if (hash == -1)
            return -1;
    }
    entry = (so->lookup)(so, key, hash);
    if (entry == NULL)
        return -1;
    if (entry->key == NULL  ||  entry->key == *reinterpret_cast<PyObject**>(dummy))
        return DISCARD_NOTFOUND;
    old_key = entry->key;
    Py_INCREF(*reinterpret_cast<PyObject**>(dummy));
    entry->key = *reinterpret_cast<PyObject**>(dummy);
    so->used--;
    Py_DECREF(old_key);
    return DISCARD_FOUND;
}

int
PySet_Discard(PyObject *set, PyObject *key)
{
    if (!PySet_Check(set)) {
        PyErr_BadInternalCall();
        return -1;
    }
    return set_discard_key((PySetObject *)set, key);
}