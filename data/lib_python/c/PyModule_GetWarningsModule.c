#define Py_BUILD_CORE 1
#include "Python.h"

#undef PyModule_GetWarningsModule
PyAPI_FUNC(PyObject *) PyModule_GetWarningsModule(void);


PyObject *
PyModule_GetWarningsModule(void)
{
    return PyImport_ImportModule("warnings");
}
