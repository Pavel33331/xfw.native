#define Py_BUILD_CORE 1

#include "Python.h"

PyAPI_FUNC(void) PySys_AddWarnOption(char *);

extern "C" size_t warnoptions;

void
PySys_AddWarnOption(char *s)
{
    PyObject *str;

    if (*reinterpret_cast<PyObject**>(warnoptions) == NULL || !PyList_Check(*reinterpret_cast<PyObject**>(warnoptions))) {
        Py_XDECREF(*reinterpret_cast<PyObject**>(warnoptions));
        *reinterpret_cast<PyObject**>(warnoptions) = PyList_New(0);
        if (*reinterpret_cast<PyObject**>(warnoptions) == NULL)
            return;
    }
    str = PyString_FromString(s);
    if (str != NULL) {
        PyList_Append(*reinterpret_cast<PyObject**>(warnoptions), str);
        Py_DECREF(str);
    }
}