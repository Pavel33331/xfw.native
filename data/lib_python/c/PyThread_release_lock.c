#define Py_BUILD_CORE 1
#include "Python.h"
#include "pythread.h"

#include <windows.h>
#include <limits.h>
#include <process.h>

typedef struct NRMUTEX {
    LONG   owned ;
    DWORD  thread_id ;
    HANDLE hevent ;
} NRMUTEX, *PNRMUTEX ;

BOOL
LeaveNonRecursiveMutex(PNRMUTEX mutex)
{
    /* We don't own the mutex */
    mutex->thread_id = 0 ;
    return
        InterlockedDecrement(&mutex->owned) < 0 ||
        SetEvent(mutex->hevent) ; /* Other threads are waiting, wake one on them up */
}

void
PyThread_release_lock(PyThread_type_lock aLock)
{
    //dprintf(("%ld: PyThread_release_lock(%p) called\n", PyThread_get_thread_ident(),aLock));

    if (!(aLock && LeaveNonRecursiveMutex((PNRMUTEX) aLock))){}
        //dprintf(("%ld: Could not PyThread_release_lock(%p) error: %ld\n", PyThread_get_thread_ident(), aLock, GetLastError()));
}