#define Py_BUILD_CORE 1
#include "Python.h"

PyAPI_FUNC(void) PyFloat_AsReprString(char*, PyFloatObject *v);

void
PyFloat_AsReprString(char *buf, PyFloatObject *v)
{
    char * tmp = PyOS_double_to_string(v->ob_fval, 'r', 0,
                    Py_DTSF_ADD_DOT_0, NULL);
    strcpy(buf, tmp);
    PyMem_Free(tmp);
}