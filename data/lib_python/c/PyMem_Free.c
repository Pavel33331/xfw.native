#define Py_BUILD_CORE 1
#include "Python.h"
#include "pymem.h"

PyAPI_FUNC(void) PyMem_Free(void *);

/* Python's malloc wrappers (see pymem.h) */

void
PyMem_Free(void *p)
{
    PyMem_FREE(p);
}