#define Py_BUILD_CORE 1
#include "Python.h"

#undef PyMapping_Length
PyAPI_FUNC(Py_ssize_t) PyMapping_Length(PyObject *o);

Py_ssize_t
PyMapping_Length(PyObject *o)
{
    return PyMapping_Size(o);
}