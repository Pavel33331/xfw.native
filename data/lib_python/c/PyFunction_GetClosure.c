#define Py_BUILD_CORE 1
#include "Python.h"

#undef PyFunction_GetClosure
PyAPI_FUNC(PyObject *) PyFunction_GetClosure(PyObject *);

PyObject *
PyFunction_GetClosure(PyObject *op)
{
    if (!PyFunction_Check(op)) {
        PyErr_BadInternalCall();
        return NULL;
    }
    return ((PyFunctionObject *) op) -> func_closure;
}