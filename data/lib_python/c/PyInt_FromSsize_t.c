#define Py_BUILD_CORE 1
#include "Python.h"

PyAPI_FUNC(PyObject *) PyInt_FromSsize_t(Py_ssize_t);

PyObject *
PyInt_FromSsize_t(Py_ssize_t ival)
{
    if (ival >= LONG_MIN && ival <= LONG_MAX)
        return PyInt_FromLong((long)ival);
    return _PyLong_FromSsize_t(ival);
}