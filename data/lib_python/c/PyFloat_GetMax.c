#define Py_BUILD_CORE 1
#include "Python.h"

PyAPI_FUNC(double) PyFloat_GetMax(void);

double
PyFloat_GetMax(void)
{
    return DBL_MAX;
}