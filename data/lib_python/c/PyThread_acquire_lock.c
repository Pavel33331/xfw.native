#define Py_BUILD_CORE 1
#include "Python.h"
#include "pythread.h"

#include "Windows.h"

//thread.c#L70
#define dprintf(args)

//thread_nt.h#L12
typedef struct NRMUTEX {
    LONG   owned;
    DWORD  thread_id;
    HANDLE hevent;
} NRMUTEX, * PNRMUTEX;

//thread_nt.h#L36
DWORD
EnterNonRecursiveMutex(PNRMUTEX mutex, BOOL wait)
{
    /* Assume that the thread waits successfully */
    DWORD ret ;

    /* InterlockedIncrement(&mutex->owned) == 0 means that no thread currently owns the mutex */
    if (!wait)
    {
        if (InterlockedCompareExchange(&mutex->owned, 0, -1) != -1)
            return WAIT_TIMEOUT ;
        ret = WAIT_OBJECT_0 ;
    }
    else
        ret = InterlockedIncrement(&mutex->owned) ?
            /* Some thread owns the mutex, let's wait... */
            WaitForSingleObject(mutex->hevent, INFINITE) : WAIT_OBJECT_0 ;

    mutex->thread_id = GetCurrentThreadId() ; /* We own it */
    return ret ;
}

int
PyThread_acquire_lock(PyThread_type_lock aLock, int waitflag)
{
    int success;

    dprintf(("%ld: PyThread_acquire_lock(%p, %d) called\n", PyThread_get_thread_ident(), aLock, waitflag));

    success = aLock && EnterNonRecursiveMutex((PNRMUTEX)aLock, (waitflag ? INFINITE : 0)) == WAIT_OBJECT_0;

    dprintf(("%ld: PyThread_acquire_lock(%p, %d) -> %d\n", PyThread_get_thread_ident(), aLock, waitflag, success));

    return success;
}