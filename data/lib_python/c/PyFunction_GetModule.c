#define Py_BUILD_CORE 1
#include "Python.h"

#undef PyFunction_GetModule
PyAPI_FUNC(PyObject *) PyFunction_GetModule(PyObject *);

PyObject *
PyFunction_GetModule(PyObject *op)
{
    if (!PyFunction_Check(op)) {
        PyErr_BadInternalCall();
        return NULL;
    }
    return ((PyFunctionObject *) op) -> func_module;
}