/**
* This file is part of the XVM project.
*
* Copyright (c) 2017-2020 XVM contributors.
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef _WIN64
#include "polyhook2/Detour/x64Detour.hpp"
#else
#include "polyhook2/Detour/x86Detour.hpp"
#endif

class FixWinapi {
public:
    FixWinapi() = delete;
    ~FixWinapi() = delete;

    static bool Initialize();
    static bool Enable();
    static bool Disable();

    static uint64_t trampoline;

private:
#ifdef _WIN64
    static PLH::x64Detour* detour;
#else
    static PLH::x86Detour* detour;
#endif
};
