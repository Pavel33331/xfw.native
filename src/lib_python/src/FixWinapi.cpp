/**
* This file is part of the XVM project.
*
* Copyright (c) 2017-2020 XVM contributors.
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <Windows.h>

#include "Libpython.h"
#include "Initializer.h"
#include "FixWinapi.h"


#include "polyhook2/ZydisDisassembler.hpp"

//////

FARPROC WINAPI GetProcAddress_libpython(HMODULE hModule, LPCSTR lpProcName)
{
    FARPROC ret = nullptr;
    if (hModule == PyWin_DLLhModule)
    {
        ret = (FARPROC)Initializer::GetRealAddress(lpProcName);
        if (ret != nullptr) {
            return ret;
        }
    }

    return PLH::FnCast(FixWinapi::trampoline, &GetProcAddress)(hModule, lpProcName);
}

//////

#ifdef _WIN64
PLH::x64Detour* FixWinapi::detour = nullptr;
#else
PLH::x86Detour* FixWinapi::detour = nullptr;
#endif

uint64_t FixWinapi::trampoline = 0;;

bool FixWinapi::Initialize() {
    if (FixWinapi::detour != nullptr) {
        return false;
    }

#ifdef _WIN64
    PLH::ZydisDisassembler dis(PLH::Mode::x64);
    detour = new PLH::x64Detour((char*)&GetProcAddress, (char*)&GetProcAddress_libpython, &trampoline, dis);
#else
    PLH::ZydisDisassembler dis(PLH::Mode::x86);
    detour = new PLH::x86Detour((char*)&GetProcAddress, (char*)&GetProcAddress_libpython, &trampoline, dis);
#endif

    return true;
}
bool FixWinapi::Enable()
{
    if (!detour) {
        return false;
    }

    return detour->hook();
}

bool FixWinapi::Disable()
{
    if (!detour) {
        return false;
    }

    return detour->unHook();
}
