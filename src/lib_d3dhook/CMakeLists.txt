# This file is part of the XVM project.
#
# Copyright (c) 2017-2020 XVM contributors.
#
# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, version 3.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required (VERSION 3.9)

project(xfw_d3dhook LANGUAGES C CXX)

set(XFW_D3DHOOK_MAJOR_VERSION 2)
set(XFW_D3DHOOK_MINOR_VERSION 7)
set(XFW_D3DHOOK_PATCH_VERSION 7)
set(XFW_D3DHOOK_VERSION ${XFW_D3DHOOK_MAJOR_VERSION}.${XFW_D3DHOOK_MINOR_VERSION}.${XFW_D3DHOOK_PATCH_VERSION})

find_package(minhook REQUIRED)
find_package(libpython REQUIRED)

include(GenerateExportHeader)
include(CMakePackageConfigHelpers)

add_library(xfw_d3dhook SHARED
    "src/direct3Dv9.h"
    "src/direct3Dv9.cpp"

    "src/direct3Dv11.h"
    "src/direct3Dv11.cpp"

    "src/xfw_d3dhook.cpp"
    "src/xfw_d3dhook.def"
)

target_include_directories(xfw_d3dhook
    PRIVATE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include/xfw_d3dhook>
    INTERFACE
    $<INSTALL_INTERFACE:include/xfw_d3dhook>
)

target_include_directories(xfw_d3dhook PRIVATE "src")

set_target_properties(xfw_d3dhook PROPERTIES CXX_STANDARD 17)

target_compile_definitions(xfw_d3dhook PRIVATE "-D_CRT_SECURE_NO_WARNINGS")
target_compile_definitions(xfw_d3dhook PRIVATE "-DNOMINMAX")

target_compile_options(xfw_d3dhook PRIVATE "/d2FH4-")

target_link_libraries(xfw_d3dhook PRIVATE minhook::minhook)
target_link_libraries(xfw_d3dhook PRIVATE libpython::python27)
target_link_libraries(xfw_d3dhook PRIVATE d3d9)
target_link_libraries(xfw_d3dhook PRIVATE d3d11)

set_target_properties(xfw_d3dhook PROPERTIES LINK_FLAGS "/INCREMENTAL:NO /DELAYLOAD:minhook.x32.dll /DELAYLOAD:d3d9.dll /DELAYLOAD:d3d11.dll")

################
# CMAKE CONFIG #
################

#generate cmake-config
configure_package_config_file(
        "cmake/xfw_d3dhook-config.cmake.in"
        "xfw_d3dhook-config.cmake"
    INSTALL_DESTINATION
        "lib/xfw_d3dhook"
)

#generate cmake-version
write_basic_package_version_file(
        "xfw_d3dhook-config-version.cmake"
    VERSION
        ${XFW_D3DHOOK_VERSION}
    COMPATIBILITY
        SameMajorVersion
)
install(
    FILES
        "${CMAKE_CURRENT_BINARY_DIR}/xfw_d3dhook-config.cmake"
        "${CMAKE_CURRENT_BINARY_DIR}/xfw_d3dhook-config-version.cmake"
    DESTINATION
        "lib/xfw_d3dhook"
)

#################
#    INSTALL    #
#################

install(TARGETS xfw_d3dhook
    EXPORT xfw_d3dhook-targets
    RUNTIME DESTINATION "bin"
    LIBRARY DESTINATION "lib"
    ARCHIVE DESTINATION "lib"
)

install(
    EXPORT
        xfw_d3dhook-targets
    NAMESPACE
        xfw_d3dhook::
    DESTINATION
        "lib/xfw_d3dhook"
)

install(
    DIRECTORY
        "include/xfw_d3dhook"
    DESTINATION
        "include"
)
